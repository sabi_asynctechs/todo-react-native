import React from "react";
import { StyleProp, StyleSheet, Text, TextStyle } from "react-native";
interface IProps {
  children: React.ReactNode;
  style?: StyleProp<TextStyle>;
  color?: string;
  font?: "thin" | "light" | "normal" | "medium" | "bold" | "extrabold";
  size?: "xs" | "sm" | "base" | "lg" | "xl" | "2xl" | "3xl"; //;
}

export const ApText: React.FC<IProps> = ({
  children,
  style,
  color,
  size = "base",
  font = "normal",
}) => {
  return (
    <Text
      style={[
        styles.text,
        styles[`size_${size}`],
        styles[`font_${font}`],
        { color },
        style,
      ]}
    >
      {children}
    </Text>
  );
};

const styles = StyleSheet.create({
  text: {
    fontFamily: "Montserrat-Regular",
  },
  size_xs: {
    fontSize: 12,
    lineHeight: 16,
  },
  size_sm: {
    fontSize: 14,
    lineHeight: 20,
  },
  size_base: {
    fontSize: 16,
    lineHeight: 24,
  },
  size_lg: {
    fontSize: 18,
    lineHeight: 28,
  },
  size_xl: {
    fontSize: 20,
    lineHeight: 28,
  },
  size_2xl: {
    fontSize: 24,
    lineHeight: 32,
  },
  size_3xl: {
    fontSize: 30,
    lineHeight: 36,
  },
  font_thin: {
    fontWeight: "100",
  },
  font_light: {
    fontWeight: "300",
    fontFamily: "Montserrat-Light",
  },
  font_normal: {
    fontWeight: "400",
  },
  font_medium: {
    fontWeight: "500",
    fontFamily: "Montserrat-Medium",
  },

  font_bold: {
    fontWeight: "700",
    fontFamily: "Montserrat-Bold",
  },
  font_extrabold: {
    fontWeight: "800",
  },
});

// font-normal	font-weight: 400;
// font-medium	font-weight: 500;
// font-semibold	font-weight: 600;
// font-bold	font-weight: 700;
// font-extrabold	font-weight: 800;
// font-black	font-weight: 900;
