import CheckBox from "expo-checkbox";
import React, { useEffect, useState } from "react";
import {
  StyleProp,
  StyleSheet,
  Text,
  TextStyle,
  TouchableOpacity,
  View,
  ViewStyle,
} from "react-native";
import normalize from "react-native-normalize";
import { ApTheme } from "../../constants";
import { ApText } from "../typography";

interface IProps {
  label: string | undefined;
  checked?: boolean;
  position?: "left" | "right";
  checkStyle?: StyleProp<ViewStyle>;
  labelStyle?: StyleProp<TextStyle>;
  onValueChange?: (val: boolean) => void;
}

export const ApCheckInput: React.FC<IProps> = ({
  label,
  position = "right",
  checked = false,
  onValueChange,
  checkStyle,
  labelStyle,
}) => {
  const [isSelected, setSelection] = useState(checked);

  useEffect(() => {
    setSelection(checked);
  }, [checked]);

  const handleValueChange = (value: boolean) => {
    setSelection(value);
    if (onValueChange) onValueChange(value);
  };

  const _renderCheck = () => (
    <CheckBox
      value={isSelected}
      onValueChange={handleValueChange}
      color={isSelected ? ApTheme.Color.primary : ""}
      style={[styles.checkbox, checkStyle]}
    />
  );

  return (
    <View style={styles.container}>
      <View style={styles.checkboxContainer}>
        {position == "left" ? _renderCheck() : null}
        <ApText style={labelStyle}>{label}</ApText>
        {position == "right" ? _renderCheck() : null}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {},
  checkboxContainer: {
    flex: 1,
    flexDirection: "row",
    marginBottom: normalize(15),
    marginTop: normalize(10),
    justifyContent: "space-between",
    minHeight: normalize(30),
    alignItems: "center",
  },
  checkbox: {
    alignSelf: "center",
  },
});
