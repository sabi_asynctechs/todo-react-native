import React from "react";
import { StyleSheet, TextInput, TextInputProps } from "react-native";

interface IProps extends TextInputProps {}

export const ApTextInput: React.FC<IProps> = (props: IProps) => {
  return <TextInput style={styles.input} {...props} />;
};

const styles = StyleSheet.create({
  input: {
    flex: 1,
    height: 50,
    borderBottomWidth: 1,
    borderBottomColor: "grey",
  },
});
