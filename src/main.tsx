import React from "react";
import { SafeAreaView, StyleSheet, TouchableOpacity, View } from "react-native";
import { ApText } from "./components";
import { ApTheme } from "./constants";
import { useAuthState } from "./modules/auth/context";
import { TodoScreen } from "./modules/todo/screen";

export const MainScreen = () => {
  const { authenticated, signin } = useAuthState();

  if (!authenticated) {
    return (
      <>
        <View style={styles.container}>
          <SafeAreaView />

          <TouchableOpacity style={styles.button} onPress={() => signin()}>
            <ApText font="bold" style={styles.buttonText}>
              Authenticate
            </ApText>
          </TouchableOpacity>
        </View>
      </>
    );
  }

  return <TodoScreen />;
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display:"flex",
    justifyContent:"center",
    alignItems:"center"
  },
  button: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: ApTheme.Color.primary,
    height: 50,
    width: "50%",
    borderRadius: 50,
  },
  buttonText: {
    color: "white",
    fontWeight: "bold",
  },
});
