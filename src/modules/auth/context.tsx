import React, { useCallback, useEffect, useState } from "react";
import * as LocalAuthentication from "expo-local-authentication";

interface IAuthState {
  authenticated: boolean;
  signin: () => void;
}

const AuthContext = React.createContext<IAuthState>({
  authenticated: false,
  signin() {},
});

const useAuthState = () => {
  return React.useContext(AuthContext);
};

interface IProps {
  children: React.ReactNode;
}

const AuthContextProvider: React.FC<IProps> = ({ children }) => {
  const [authenticated, setAuthenticated] = useState(false);

  const authenticate = useCallback(() => {
    async () => {
      await signin();
    };
  }, []);

  useEffect(() => {
    if (!authenticated) authenticate();
  }, [authenticate]);

  const signin = async () => {
    const auth = await LocalAuthentication.authenticateAsync({});
    setAuthenticated(auth.success);
  };

  return (
    <AuthContext.Provider value={{ authenticated, signin }}>
      {children}
    </AuthContext.Provider>
  );
};

export { AuthContextProvider, useAuthState };
