import React from "react";
import { SafeAreaView, StyleSheet, Text, View } from "react-native";
import { ApText } from "../../components";
import { ApTheme } from "../../constants";
import { TodoInput } from "./components/input";
import { TodoListItem } from "./components/item";
import { useTodoState } from "./context";
import { ITodo } from "./model";

export const TodoScreen = () => {
  const { todos, todo, selectedTodo, updateTodo, removeTodo } = useTodoState();

  const handleOnUpdate = (value: string) => {
    updateTodo({ ...todo, text: value } as any);
  };

  const handleDeleteTodo = (todo: ITodo) => {
    removeTodo(todo.id);
  };

  return (
    <View style={styles.container}>
      <View>
        <SafeAreaView />
        <ApText font="bold" style={styles.heading}>
          TODO: ({todos?.filter((t) => t.completed)?.length} / {todos?.length})
        </ApText>

        {todos.map((todo, i) => (
          <TodoListItem
            onPress={selectedTodo}
            onRemove={handleDeleteTodo}
            todo={todo}
            key={i}
          />
        ))}
      </View>

      <TodoInput
        id={todo?.id?.toString()}
        value={todo?.text}
        onUpdate={handleOnUpdate}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
    padding: 15,
    backgroundColor: "#f4f4f5",
  },
  heading: {
    fontSize: 24,
    color: ApTheme.Color.primary,
    fontWeight: "bold",
    marginTop: 30,
    marginBottom: 22,
  },
});
