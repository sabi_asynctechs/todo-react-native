import React, { useState } from "react";
import { ITodo } from "./model";

// intitial data.
const data = [
  { id: 1, text: "First Item" },
  { id: 2, text: "Second Item" },
  { id: 3, text: "Third Item" },
];

interface ITodoState {
  todo?: ITodo;
  todos: ITodo[];
  selectedTodo: (todo: ITodo) => void;
  updateTodo: (todo:  ITodo) => void;
  removeTodo: (id: number) => void;
}

const TodoContext = React.createContext<ITodoState>({
  todos: [],
  selectedTodo(todo) {},
  updateTodo(todo) {
  },
  removeTodo(id) {},
});

const useTodoState = () => {
  return React.useContext(TodoContext);
};

interface IProps {
  children: React.ReactNode;
}

const TodoContextProvider: React.FC<IProps> = ({ children }) => {
  const [todos, setTodos] = useState<ITodo[]>(data);
  const [todo, setTodo] = useState<ITodo>();

  // handle add new and update todo
  const updateTodo = (payload:  ITodo) => {
    if (payload.id) {
      setTodos(
        todos.map((t) => (t.id === payload.id ? payload : t))
      );
    } else setTodos([...todos, { id: Date.now(), text: payload.text }]);

    setTodo(undefined);
  };

  // handle remove todo item
  const removeTodo = (id: number) => {
    setTodos(todos.filter((t) => t.id !== id));
  };

  // update selected todo to todo state
  const selectedTodo = (payload: ITodo) => {
    setTodo(payload);
  };


  return (
    <TodoContext.Provider
      value={{ selectedTodo, todos, todo, updateTodo, removeTodo }}
    >
      {children}
    </TodoContext.Provider>
  );
};

export { TodoContextProvider, useTodoState };
