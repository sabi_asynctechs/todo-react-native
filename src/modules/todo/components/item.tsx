import React from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { ApCheckInput, ApText } from "../../../components";
import { ApTheme } from "../../../constants";
import { useTodoState } from "../context";
import { ITodo } from "../model";

interface IProps {
  todo: ITodo;
  onPress?: (todo: ITodo) => void;
  onRemove?: (todo: ITodo) => void;
}

export const TodoListItem: React.FC<IProps> = ({ todo, onPress, onRemove }) => {
  const { updateTodo } = useTodoState();

  const handleCheck = (value: boolean) => {
    updateTodo({ ...todo, completed: value });
  };

  return (
    <TouchableOpacity
      style={styles.container}
      onPress={() => {
        if (onPress) onPress(todo);
      }}
    >
      <ApCheckInput
        label={todo?.text}
        position="left"
        checked={todo.completed}
        checkStyle={styles.check}
        labelStyle={todo.completed && styles.completed}
        onValueChange={handleCheck}
      />

      <TouchableOpacity
        onPress={() => {
          if (onRemove) onRemove(todo);
        }}
      >
        <ApText font="medium">REMOVE</ApText>
      </TouchableOpacity>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 50,
    borderRadius: 20,
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    padding: 5,
    paddingLeft: 15,
    paddingRight: 15,
    marginBottom: 10,
    backgroundColor: "white",
  },
  todoContainer: {
    display: "flex",
    flexDirection: "row",
  },
  text: {
    fontSize: 16,
  },
  completed: {
    textDecorationLine: "line-through",
    fontStyle: "italic",
  },
  check: {
    width: 20,
    height: 20,
    marginRight: 10,
    borderRadius: 50,
    borderWidth: 0,
    backgroundColor: ApTheme.Color.primary,
  },
});
