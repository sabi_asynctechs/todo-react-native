import React, { useEffect, useState } from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { ApText, ApTextInput } from "../../../components";
import { ApTheme } from "../../../constants";

interface IProps {
  id: string | undefined;
  value: string | undefined;
  onUpdate: (value: string) => void;
}

export const TodoInput: React.FC<IProps> = ({ id, value, onUpdate }) => {
  const [change, setChange] = useState(value || "");

  useEffect(() => {
    setChange(value || "");
  }, [value]);

  const handleAddOrUpdate = () => {
    onUpdate(change);
    setChange("");
  };

  return (
    <View style={styles.container}>
      <ApTextInput
        value={change}
        placeholder="Enter here"
        style={styles.input}
        onChangeText={setChange}
      />
      <TouchableOpacity style={styles.button} onPress={handleAddOrUpdate}>
        <ApText font="bold" style={styles.buttonText}>{id ? "UPDATE" : "ADD"}</ApText>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 90,
    borderRadius: 20,
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    padding: 5,
    paddingLeft: 15,
    paddingRight: 15,
    marginBottom: 10,
    backgroundColor: "white",
  },
  input: {
    height: 50,
    width: "70%",
    borderBottomWidth: 1,
    borderBottomColor: "#cbd5e1",
  },
  button: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor:  ApTheme.Color.primary,
    height: 50,
    width: 80,
    borderRadius: 50,
  },
  buttonText: {
    color: "white",
    fontWeight: "bold",
  },
});
