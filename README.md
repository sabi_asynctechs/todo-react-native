
Folder Structure

src: This folder is the main container of main code for the application
    components: This folder contain all our share components.
    modules: This folder contain all app modules
        components: This folder contain component related to only this module
        context.tsx: contain module context code.
        model.tsx: contain module interfaces and classed
        screen.tsx: module main screen page
    constants.ts: app constants


Install Dependencies
- yarn install

Run app
- yarn start: start app
- yarn android: start android app
- yarn ios: start ios app