import { useFonts } from "expo-font";
import React from "react";
import { MainScreen } from "./src/main";
import { AuthContextProvider } from "./src/modules/auth/context";
import { TodoContextProvider } from "./src/modules/todo/context";

export default function App() {
  // configure font‚
  const [fontsLoaded] = useFonts({
    Montserrat: require("./assets/fonts/Montserrat.ttf"),
    "Montserrat-Bold": require("./assets/fonts/Montserrat-Bold.ttf"),
    "Montserrat-Light": require("./assets/fonts/Montserrat-Light.ttf"),
    "Montserrat-Medium": require("./assets/fonts/Montserrat-Medium.ttf"),
    "Montserrat-Regular": require("./assets/fonts/Montserrat-Regular.ttf"),
  });

  if (!fontsLoaded) {
    return null;
  }

  return (
    <AuthContextProvider>
      <TodoContextProvider>
        <MainScreen />
      </TodoContextProvider>
    </AuthContextProvider>
  );
}
